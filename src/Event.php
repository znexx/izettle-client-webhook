<?php

namespace znexx\iZettle\webhook;

require_once __DIR__ . '/Model.php';
require_once __DIR__ . '/ProductUpdated.php';
require_once __DIR__ . '/PurchaseCreated.php';

class Event extends Model {

	public function getFieldSpecifications(): array {
		return [
			'organizationUuid' => 'string',
			'messageUuid' => 'string',
			'eventName' => 'string',
			'messageId' => 'string',
			'payload' => 'string',
			'timestamp' => 'string',
		];
	}

	public function calculateSignature(string $key): string {
		$timestampPayload = stripslashes($this->timestamp . '.' . $this->payload);
		return hash_hmac('sha256', $timestampPayload, $key);
	}

	public function isValid(string $signature, string $signingKey): bool {
		$calculatedSignature = $this->calculateSignature($signingKey);
		return $signature === $calculatedSignature;
	}
}
