<?php

namespace znexx\iZettle\webhook\PurchaseCreated;

require_once __DIR__ . '/../Model.php';

use znexx\iZettle\webhook\Model;

class Reference extends Model {

	function __construct($data = []) {
		parent::__construct($data);
	}

	public function getFieldSpecifications(): array {
		return [
			'checkoutUUID' => 'string',
		];
	}
}
