<?php

namespace znexx\iZettle\webhook\PurchaseCreated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/../Price.php';

use znexx\iZettle\webhook\Model;

class PaymentAttributes extends Model {

	function getFieldSpecifications(): array {
		return [
			'transactionStatusInformation' => 'string',
			'maskedPan' =>                    'string',
			'cardPaymentEntryMode' =>         'string',
			'referenceNumber' =>              'string',
			'cardType' =>                     'string',
			'terminalVerificationResults' =>  'string',
			'applicationIdentifier' =>        'string',
			'applicationName' =>              'string',
		];
	}
}
