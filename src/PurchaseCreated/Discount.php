<?php

namespace znexx\iZettle\webhook\PurchaseCreated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/../Price.php';

use znexx\iZettle\webhook\Model;

class Discount extends Model {

	public function getFieldSpecifications(): array {
		return [
			'uuid' => 'string',
			'name' => 'string',
			'description' => 'string',
			'amount' => '\SyntaxSociety\Price',
			'percentage' => 'string',
			'imageLookupKeys' => 'string[]',
			'externalReference' => 'string',
			'etag' => 'string',
			'updated' => 'string',
			'updatedBy' => 'string',
			'created' => 'string',
		];
	}
}
