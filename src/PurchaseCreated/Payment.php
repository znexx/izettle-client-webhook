<?php

namespace znexx\iZettle\webhook\PurchaseCreated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/PaymentAttributes.php';

use znexx\iZettle\webhook\Model;

class Payment extends Model {
	private static $fields = [
		'uuid' =>       'string',
		'amount' =>     'string',
		'type' =>       'string',
		'createdAt' =>  'string',
		'attributes' => __NAMESPACE__ . '\PaymentAttributes',
	];

	function getFieldSpecifications(): array {
		return self::$fields;
	}
}
