<?php

namespace znexx\iZettle\webhook;

require_once __DIR__ . '/Model.php';

class VariantPrice extends Model {
	public function getFieldSpecifications(): array {
		return [
			'amount' => 'int',
			'currencyId' => 'string',
		];
	}
}
