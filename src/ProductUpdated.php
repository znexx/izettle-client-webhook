<?php

namespace znexx\iZettle\webhook;

require_once __DIR__ . '/Model.php';
require_once __DIR__ . '/ProductUpdated/Product.php';

use znexx\iZettle\webhook\Model;

class ProductUpdated extends Model {

	public function getFieldSpecifications(): array {
		return [
			'organizationUuid' => 'string',
			'newEntity' => __NAMESPACE__ . '\ProductUpdated\Product',
			'oldEntity' => __NAMESPACE__ . '\ProductUpdated\Product',
		];
	}
}
