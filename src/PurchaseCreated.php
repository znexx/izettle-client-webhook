<?php

namespace znexx\iZettle\webhook;

require_once __DIR__ . '/Model.php';
require_once __DIR__ . '/PurchaseCreated/Discount.php';
require_once __DIR__ . '/PurchaseCreated/Payment.php';
require_once __DIR__ . '/PurchaseCreated/Product.php';
require_once __DIR__ . '/PurchaseCreated/Reference.php';

use znexx\iZettle\webhook\Model;
use znexx\iZettle\webhook\Payload;

class PurchaseCreated extends Model {

	function getFieldSpecifications(): array {
		return [
			'purchaseUuid' =>     'string',
			'source' =>           'string',
			'userUuid' =>         'string',
			'currency' =>         'string',
			'country' =>          'string',
			'amount' =>           'int',
			'vatAmount' =>        'int',
			'timestamp' =>        'int',
			'created' =>          'string',
			'purchaseNumber' =>   'int',
			'userDisplayName' =>  'string',
			'udid' =>             'string',
			'organizationUuid' => 'string',
			'products' =>         __NAMESPACE__ . '\PurchaseCreated\Product[]',
			'discounts' =>        __NAMESPACE__ . '\PurchaseCreated\Discount[]',
			'payments' =>         __NAMESPACE__ . '\PurchaseCreated\Payment[]',
			'references' =>       __NAMESPACE__ . '\PurchaseCreated\Reference',
		];
	}
}
