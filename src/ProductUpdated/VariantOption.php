<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';

use znexx\iZettle\webhook\Model;

class VariantOption extends Model {

	public function getFieldSpecifications(): array {
		return [
			'name' => 'string',
			'value' => 'string',
		];
	}
}
