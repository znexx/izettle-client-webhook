<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/../Price.php';
require_once __DIR__ . '/VariantOption.php';

use znexx\iZettle\webhook\Model;

class Variant extends Model {

	public function getFieldSpecifications(): array {
		return [
			'uuid' => 'string',
			'name' => 'string',
			'price' => __NAMESPACE__ . '\VariantPrice',
			'costPrice' => __NAMESPACE__ . '\VariantPrice',
			'options' => __NAMESPACE__ . '\VariantOption[]',
		];
	}
}
