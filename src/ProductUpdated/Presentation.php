<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';

use znexx\iZettle\webhook\Model;

class Presentation extends Model {
	public function getFieldSpecifications(): array {
		return [
			'imageUrl' => 'string',
			'backgroundColor' => 'string',
			'textColor' => 'string',
		];
	}
}
