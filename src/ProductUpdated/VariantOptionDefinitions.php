<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/VariantOptionDefinition.php';

use znexx\iZettle\webhook\Model;

class VariantOptionDefinitions extends Model {

	public function getFieldSpecifications(): array {
		return [
			'definitions' => __NAMESPACE__ . '\VariantOptionDefinition[]',
		];
	}
}
