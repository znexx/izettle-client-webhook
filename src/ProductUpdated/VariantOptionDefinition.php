<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/VariantOptionProperties.php';

use znexx\iZettle\webhook\Model;

class VariantOptionDefinition extends Model {

	public function getFieldSpecifications(): array {
		return [
			'name' => 'string',
			'properties' => __NAMESPACE__ . '\VariantOptionProperties[]',
		];
	}
}
