<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';
require_once __DIR__ . '/Presentation.php';
require_once __DIR__ . '/Variant.php';
require_once __DIR__ . '/VariantOptionDefinitions.php';

use znexx\iZettle\webhook\Model;

class Product extends Model {
	public function getFieldSpecifications(): array {
		return [
			'uuid' => 'string',
			'organizationUuid' => 'string',
			'name' => 'string',
			'presentation' => __NAMESPACE__ . '\Presentation',
			'variants' => __NAMESPACE__ . '\Variant[]',
			'vatPercentage' => 'string',
			'variantOptionDefinitions' => __NAMESPACE__ . '\VariantOptionDefinitions',
			'etag' => 'string',
			'updated' => 'string',
			'updatedByUserUuid' => 'string',
			'created' => 'string',
		];
	}
}
