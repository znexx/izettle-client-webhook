<?php

namespace znexx\iZettle\webhook\ProductUpdated;

require_once __DIR__ . '/../Model.php';

use znexx\iZettle\webhook\Model;

class VariantOptionProperties extends Model {

	public function getFieldSpecifications(): array {
		return [
			'value' => 'string',
			'imageUrl' => 'string',
		];
	}
}
