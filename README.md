# izettle-client-webhook

This is a bunch of PHP classes modelling the requests received from iZettles webhook API.

For more information, please visit [http://www.izettle.com](http://www.izettle.com)

## Requirements

PHP 5.5 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://bitbucket.org/znexx/izettle-client-webhook.git"
    }
  ],
  "require": {
    "znexx/izettle-client-webhook": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/izettle-client-webhook/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php

require_once(__DIR__ . '/vendor/autoload.php');

// insert your signing key
$signingKey = '...';

// Fetch the signature from the headers
$headers = apache_request_headers();
$signature = $headers['X-iZettle-Signature'];

// Read the request body
$body = file_get_contents('php://input');

// Parse into an array
$eventData = json_decode($body, true);

// Deserialize into an Event object
$event = new Event($eventData);

if ($event->isValid($signature, $signingKey)) {
	echo "Valid " . $event->eventName . " request!";
} else {
	echo "Invalid request!";
}

?>
```

## Author

z-nexx,
znexxpwnz0r@gmail.com
